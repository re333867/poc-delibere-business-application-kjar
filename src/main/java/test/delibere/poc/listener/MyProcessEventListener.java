package test.delibere.poc.listener;

import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.event.process.ProcessNodeLeftEvent;
import org.kie.api.event.process.ProcessNodeTriggeredEvent;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.process.ProcessVariableChangedEvent;

 
/**
 * 
 * Nota: possibile estendere anche org.kie.api.event.process.DefaultProcessEventListener
 * per evitare implementazione di tutti i metodi 
 * 
 * 
 * @author R.Esposito
 *
 */
public class MyProcessEventListener implements ProcessEventListener{

		
	@Override
	public void beforeProcessStarted(ProcessStartedEvent ev) {		
		System.out.println("MyProcessEventListener - start beforeProcessStarted "+ev.getProcessInstance().getId()+" "+ev.getProcessInstance().getProcessName());	
	}
	
	@Override
	public void afterProcessStarted(ProcessStartedEvent ev) {
		System.out.println("MyProcessEventListener - start afterProcessCompleted "+ev.getProcessInstance().getId()+" "+ev.getProcessInstance().getProcessName());								
	}
	
	@Override
	public void afterProcessCompleted(ProcessCompletedEvent ev) {
		System.out.println("MyProcessEventListener - start afterProcessCompleted "+ev.getProcessInstance().getId()+" "+ev.getProcessInstance().getProcessName());				
	}
	
	@Override
	public void afterNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

	

	@Override
	public void afterVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeProcessCompleted(ProcessCompletedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void beforeVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
